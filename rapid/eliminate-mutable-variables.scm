;; Copyright (C) 2017 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;> \procedure{(eliminate-mutable-variables expr)}

;;> Boxes all mutable variables and eliminates all assignments to
;;> variables.  It takes a syntax object encapsulating an expression
;;> of the input language and returns a syntax object encapsulating an
;;> expression of the output language.

;;> The input language has the following expressions: \scheme{(begin
;;> ...)}, \scheme{(if ...)}, \scheme{(set! ...)},
;;> \scheme{(set-values! ...)}, \scheme{(quote
;;> ...)}, \scheme{(case-lambda ...)}, \scheme{(letrec ...)},
;;> \scheme{(let-values ...)}, procedure calls, and variable references.

;;> The output language has the following expressions: \scheme{(begin
;;> ...)}, \scheme{(if ...)}, \scheme{(quote ...)},
;;> \scheme{(case-lambda ...)}, \scheme{(letrec ...)},
;;> \scheme{(let-values ...)}, procedure calls, and variable
;;> references.

(define (eliminate-mutable-variables expr)
  (receive (mutable-variables)
      (get-mutable-variables expr)
    (define (mutable? var)
      (set-contains? mutable-variables var))
    (syntax-match expr
      ((begin ,(expr*) ...)
       (syntax (begin ,expr* ...)))
      ((if ,(test) ,(consequent) ,(alternate))
       (syntax (if ,test ,consequent ,alternate)))
      ((quote ,literal)
       (syntax (quote ,literal)))
      ((case-lambda (,formals ,(body) ...))
       (syntax (case-lambda (,formals ,body) ...)))
      ((let-values ((,formals ,(init))) ,(body))
       (syntax (let-values ((,formals ,init)) ,body)))
      ((letrec ((,var ,(init)) ...) ,(body))
       (syntax (letrec ((,var ,init) ...) ,body)))
      ((set-values! (,arg1* ... . ,arg2*) ,(expr))
       (if (and (null? arg2*)
		(pair? arg1*)
		(null? (cdr arg1*)))
	   (syntax (box-set! ,(car arg1*) ,expr))
	   (let* ((%arg2* (if (null? arg2*) '() (list arg2*)))
		  (tmp1* (map (lambda (arg1)
				(gensym 'tmp))
			      arg1*))
		  (%tmp2* (map (lambda (arg2)
				 (gensym 'tmp))
			       %arg2*))
		  (tmp2* (if (null? %tmp2*) '() (car %tmp2*))))	    
	     (syntax
	      (let-values (((,tmp1* ... . ,tmp2*) ,expr))
		(begin
		  (box-set! ,arg1* ,tmp1*) ...
		  (box-set! ,%arg2* ,%tmp2*) ...))))))
      ((,(expr) ...)
       (syntax (,expr ...)))
      (,expr
       (if (mutable? (unwrap-syntax expr))
	   (syntax (unbox ,expr))
	   expr)))))

(define (get-mutable-variables expr)
  (syntax-match expr
    ((begin ,(vars*) ...)
     (set-union* vars*))
    ((if ,(test-vars) ,(consequent-vars) ,(alternate-vars))
     (set-union test-vars consequent-vars alternate-vars))
    ((quote ,literal)
     (set symbol-comparator))
    ((set! ,(unwrap-syntax -> var) ,(vars))
     (set-adjoin vars var))
    ((set-values! (,(unwrap-syntax -> arg1*) ... . ,(unwrap-syntax -> arg2*)) ,(vars))
     (set-adjoin* vars arg1* arg2*))
    ((case-lambda ((,formals ,(body-vars*)) ...))
     (set-union* body-vars*))
    ((let-values ((,formals ,(init-vars))) ,(body-vars))
     (set-union init-vars body-vars))
    ((letrec ((,formals* ,(init-vars*)) ...) ,(body-vars))
     (set-union (set-union* init-vars*)
		body-vars))
    ((,(vars*) ...)
     (set-union* vars*))
    (,expr
     (set symbol-comparator))))

;;; Utility procedures

(define (set-adjoin* set element1* element2*)
  (let ((set (apply set-adjoin set element1*)))
    (if (null? element2*)
	set
	(set-adjoin set element2*))))

(define (set-union* set*)
  (apply set-union set*))
