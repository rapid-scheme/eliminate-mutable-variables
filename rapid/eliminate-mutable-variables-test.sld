;; Copyright (C) 2017 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-library (rapid eliminate-mutable-variables-test)
  (export run-tests)
  (import (scheme base)            
	  (rapid test)
	  (rapid syntax)
	  (rapid eliminate-mutable-variables))
  (begin
    (define (run-tests)
      (test-begin "Eliminate mutable variables")

      (test-group "Examples"
	(define expr1
	  (syntax (let-values (((a) '1))
		    (begin (set-values! (a) '2)
			   a))))

	(define expr2
	  (syntax (let-values (((a) '1))
		    (let-values (((b) '2))
		      (begin (set-values! (a . b) (f))
			     a)))))

	(test-equal "Simple assignment"
	  '(let-values (((a) (quote 1)))
	     (begin
	       (box-set! a (quote 2))
	       (unbox a)))
	  (syntax->datum (eliminate-mutable-variables expr1)))

	(test-equal "Multiple assignment"
	  '(let-values (((a) (quote 1)))
	     (let-values (((b) (quote 2)))
	       (begin
		 (let-values (((tmp.0 . tmp.1) (f)))
		   (begin
		     (box-set! a tmp.0)
		     (box-set! b tmp.1)))
		 (unbox a))))
	  (syntax->datum (eliminate-mutable-variables expr2))))

      (test-end))))
